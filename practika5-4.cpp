#include "stdafx.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#define DLINA 80
#define COUNT_WORD 40

void printWord(char *ptr[], int count_ptr)
{
    int count = count_ptr;
    FILE *file_out;
    file_out = fopen("str_out.txt", "a+");
    while(count > 0)
    {
        int count_sign = 0;
        int r_digit = rand() % count_ptr;
        if(ptr[r_digit] == 0)
          continue;
        else
          while(ptr[r_digit][count_sign] != '\n' && ptr[r_digit][count_sign] != ' ')
            fprintf(file_out, "%c",ptr[r_digit][count_sign++]);
        fprintf(file_out," ");
        ptr[r_digit] = 0;
        count--;
    }
    fprintf(file_out,"\n");
    fclose(file_out);
}
int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    char sign[DLINA];
    char *ptr_sign[COUNT_WORD];
    FILE *file_in;
    file_in = fopen("str_in.txt", "r+");
    while(fgets(sign, DLINA, file_in))
    {
        int length, count_ptr = 0;
        length = strlen(sign);
        bool count = true;
        for(int i = 0; i < length; i++)
        {
            if(count == true && isalpha(sign[i]))
            {
                ptr_sign[count_ptr] = &sign[i];
                count_ptr++;
                count = false;
            }
            else if(isspace(sign[i]))
              count = true;
        }
        printWord(ptr_sign, count_ptr);
    }
    fclose(file_in);
    return 0;
}