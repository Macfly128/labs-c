#include <stdio.h>
#define DLINNA 256
int main()
{
    char string[DLINNA];
    int count_s[DLINNA] = {0};
    int count;
    scanf("%255s", string);
    for(count = 0; count < DLINNA; count++)
    {
        count_s[string[count]]++;

        if(count_s[count])
          printf("%c - %d\n", count, count_s[count]);
    }
    return 0;
}