#include "stdafx.h"
#include <string.h>
#include <ctype.h>
#define STRING 80
int main()
{
    char sign[STRING];
    int count_ch,count_dig;
    while((scanf("%79s", sign)) != EOF)
    {
        int length = strlen(sign);
        for(count_ch = 0; count_ch < length; count_ch++)
        {
            if(!isdigit(sign[count_ch]))
            {
                for(count_dig = 0; count_dig < count_ch; count_dig++)
                {
                    if(isdigit(sign[count_dig]))
                    {
                        char save_ch;
                        save_ch = sign[count_dig];
                        sign[count_dig] = sign[count_ch];
                        sign[count_ch] = save_ch;
                    }
                }
            }
        }
          printf("%s\n", sign);
    }
    return 0;
}