#include <stdio.h>
int main()
{
	int b;
	const float grad = 180.00, pi = 3.1416;
	float znach_u, x;
	char rd;
	printf("Vvedite znachenie ugla(00.00R/D):");
	b = scanf("%f %c", &znach_u, &rd);
	while(b)
	{
      if(rd == 'D')
	  {
	 x = znach_u * (grad / pi);
	 printf("Znachenie ugla v gradusah %.2f\n Znachenie ugla v radianah %.1f\n", znach_u, x);
	  }
	  else if (rd == 'R')
	  {
     x = znach_u * (pi / grad);
	 printf("Znachenie ugla v radianah %.2f\n Znachenie ugla v gradusah %.1f\n", znach_u, x);
	  }
	  else
	  printf("Vi vveli nepravilnoe znachenie!\n");
	  printf("Vvedite znachenie ugla(00.00R/D):"); 
	  b = scanf("%f %c", &znach_u, &rd);
	}
	return 0;
}