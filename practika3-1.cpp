#include "stdafx.h"
#include <ctype.h>

int _tmain(int argc, _TCHAR* argv[])
{
    char sign;
    int count_word = 0;
    bool count = true;
    while((sign = getchar()) != '\n')
    {
        if(count == true && isalpha(sign))
        {
          count_word++;
          count = false;
        }

        else if(isspace(sign))
          count = true;

        putchar(sign);
    }
    printf("\nKol-vo slov = %d\n", count_word);
    return 0;
}