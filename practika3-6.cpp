#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#define DLINA 20

int _tmain(int argc, _TCHAR* argv[])
{
    int sign[DLINA];
    int min = 0,max = 0;
    int min_i, max_i,sum = 0;
    int count = 0;
    int i;
    srand(time(NULL));
    printf("Znacheniya massiva:\n");
    for(i = 0; i < DLINA; i++)
    {
        sign[i] = 1 + rand() % 100;

        if(i == 0)
          min = sign[i];

        if(sign[i] > max)
        {
            max = sign[i];
            max_i = i;
        }
        else if(sign[i] < min)
        {
            min = sign[i];
            min_i = i;
        }
        printf("%3d", sign[i]);
    }

    if(max_i > min_i)
      for(i = min_i; i <= max_i; i++)
        sum += sign[i];
    else
      for(i = max_i; i <= min_i; i++)
        sum += sign[i];

    printf("\nmin %d, max %d\nSumma mezdu max i min elementami %d\n",min, max, sum);
    return 0;
}