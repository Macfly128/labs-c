#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#define DLINA 80
#define COUNT_WORD 40
#define LENGTH_WORD 20

int sign_in_wordRand(char *ptr)
{
    char word[LENGTH_WORD];
    int count_sign = 0;
    while(isalpha(ptr[count_sign]))
    {
        word[count_sign] = ptr[count_sign];
        count_sign++;
    }
    for(int i = 1; i < count_sign - 1; i++)
    {
        int r_digit = 1 + rand() % (count_sign - 1);
        if(word[r_digit] == 0)
        {
            i--;
            continue;
        }
        else
          ptr[i] = word[r_digit];
        word[r_digit] = 0;
    }
    return count_sign - 1;
}
int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    char sign[DLINA];
    FILE *file_in;
    file_in = fopen("str_in.txt", "r+");
    while(fgets(sign, DLINA, file_in))
    {
        int length = strlen(sign);
        for(int i = 0; i < length; i++)
        {
            if(isalpha(sign[i]))
            {
                int count_sign;
                count_sign = sign_in_wordRand(&sign[i]);
                i += count_sign;
            }
        }
        FILE *file_out;
        file_out = fopen("str_out.txt", "a+");
        fputs(sign, file_out);
        fclose(file_out);
    }
    fclose(file_in);
    return 0;
}
