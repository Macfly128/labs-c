#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#define DLINA 80

int comp (const void *ptr1, const void *ptr2)
{
    return -(strlen(*(char**)ptr1) - strlen(*(char**)ptr2));
}

int _tmain(int argc, _TCHAR* argv[])
{
    char strings[DLINA][DLINA];
    char *ptr_sort[DLINA];
    FILE *file;
    int count = 0;
    file = fopen("string.txt", "r");
    while(count < DLINA && fgets(strings[count], DLINA, file) && strings[count][0] != '\n')
    {
        ptr_sort[count] = strings[count];
        count++;
    }
    qsort(ptr_sort, count, sizeof(char*), comp);
    fclose(file);
    file = fopen("string.txt", "w"); 
    for(int i = 0; i < count; i++)
      fputs(ptr_sort[i], file);
    fclose(file);
    return 0;
}