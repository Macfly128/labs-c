#include "stdafx.h"
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <windows.h>

#define TWO 2

int sum_rec(int arr[], int count)
{
    int sum = 0;
    if(count >= 0)
    sum = arr[count] + sum_rec(arr,count-1);
    return sum;
}

int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    clock_t begin,end;
    double tsum_time, rsum_time;
    int *dyn_arr;
    float degree;
    int size_arr, sum_t = 0, sum_r = 0;
    
    printf("Vvedite znachenie stepeni dvoyki\n");     //ПОЛУЧАЕМ СТЕПЕРЬ ДВОЙКИ
    while(!scanf("%f", &degree))
    {
        printf("Vi vveli ne pravilnoe znachenie\n");
        fflush(stdin);
    }
    fflush(stdin);

    size_arr = pow(TWO, degree);                     //ПОЛУЧАЕМ ЗНАЧЕНИЕ СТЕПЕНИ и СОЗДАЁМ МАССИВ
    dyn_arr = (int*)malloc(size_arr * sizeof(int));

    for(int i = 0; i < size_arr; i++)               //ЗАПОЛНЯЕМ СЛУЧАЙНЫМИ ЗНАЧЕНИЯМИ
      dyn_arr[i] = 1 + rand() % 100;

    begin = clock();
    for(int i = 0; i < size_arr; i++)               //СЧИТАМ СУММУ ОБЫЧНЫМ СПОСОБОМ
      sum_t = sum_t + dyn_arr[i];
    end = clock();
    
    tsum_time = (double)(end - begin)/CLOCKS_PER_SEC;

    begin = clock();
    sum_r = sum_rec(dyn_arr, size_arr-1);           //СЧИТАМ СУММУ РЕКУРСИЕЙ
    end = clock();
    
    rsum_time =(double)(end - begin)/CLOCKS_PER_SEC;

    printf("Summa elementov tradicionnim sposobom %d i vremya %.3lf\n",sum_t, tsum_time);
    printf("Summa elementov rekursiei %d i vremya %.3lf\n", sum_r, rsum_time);

    return 0;
}