#include "stdafx.h"
#include <ctype.h>
#include <string.h>
#define DLINA 80
#define WORD 40

int _tmain(int argc, _TCHAR* argv[])
{
    char sign[DLINA];
    int arr[WORD];
    int length,count_arr = 0;
    int i;
    bool count = true;
    printf("Vvedite neskolko slov\n");
    fgets(sign, DLINA, stdin);
    length = strlen(sign);
    for(i = 0; i < length; i++) // Тут определяем кол-во слов.
    {
        if(count == true && isalpha(sign[i]))
        {
            arr[count_arr++] = i;  // Тут запоминаем начало каждого слова.
            count = false;
        }
        else if(isspace(sign[i]))
          count = true;
    }

    fflush(stdin);
    printf("\nKol-vo slov = %d\nViberite slovo ot 1 do %d:", count_arr, count_arr);
    if(int current = scanf("%d", &i) && i > 0 && i <= count_arr)
    {
        if(i == count_arr)              //Условие для последнего слова
          sign[arr[i-1]] = '\0';
        else
          while(arr[i] <= length)
          {
              sign[arr[i-1]] = sign[arr[i]];
              arr[i-1]++;
              arr[i]++;
          }

        printf("%s\n", sign);

    }
    else
      printf("Vi vveli nepravilnoe znachenie\n");

    return 0;
}