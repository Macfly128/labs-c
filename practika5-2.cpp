#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <Windows.h>
#define MAX 24

int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    char sign[MAX][MAX];
    while(1)
    {
        system("cls");

        for(int w = 0;w < MAX; w++)         //ПРОБЕЛЫ
          for(int h = 0; h < MAX; h++)
            sign[w][h] = ' ';

        for(int w = 0; w < MAX / 2; w++)    //ЗАПОЛНЕНИЕ ЛЕВОГО ВЕРХНЕГО
          for(int h = 0; h < MAX / 2; h++)
          {
              int r_digit = rand() % 2;
              if(r_digit)
                sign[w][h] = '*';
          }

        for(int w = 0; w < MAX / 2; w++)    //ЗАПОЛНЕНИЕ ПРАВОГО ВЕРХНЕГО
          for(int h = MAX / 2; h < MAX; h++)
            sign[w][h] = sign[w][MAX - h - 1];

        for(int w = MAX/2; w < MAX; w++)    //ЗАПОЛНЕНИЕ НИЖНЕЙ ЧАСТИ
          for(int h = 0; h < MAX; h++)
            sign[w][h] = sign[MAX - w - 1][h];

        for(int w = 0; w < MAX; w++)       //РАСПЕЧАТАТЬ
        {
            for(int h = 0; h < MAX; h++)
              printf("%c", sign[w][h]);
            printf("\n"); 
        }

        Sleep(1000);
    }
    return 0;
}