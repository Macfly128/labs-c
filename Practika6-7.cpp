#include "stdafx.h"
#include <stdlib.h>
#include <Windows.h>

#define H 9
#define W 29

char lab[H][W] = {"############################",
                  "#           #   #          #",
                  "##########  #   #          #",
                  "#           #   #######  ###",
                  "# ######    #              #",
                  "#      #    #   ######    ##",
                  "#####  #    #   #         ##",
                  "       #        #     ######",
                  "############################"};

void printLab(int h,int w)
{
    int i,j;
    for (i = 0; i < H; i++)
    {
        for (j = 0; j < W; j++)
        {
            if(i == h && j == w)
              putchar(lab[i][j]);

            else if(lab[i][j] == 'x')
            {
                lab[i][j] = ' ';
                putchar(lab[i][j]);
                lab[i][j] = 'x';
            }
            else 
              putchar(lab[i][j]);
        }
        putchar('\n');
    }
}
void exit_lab(int w, int h)
{
    if(w == 0 && lab[h][w] == ' ')
    {
        lab[h][w] = 'x';
        printLab(h, w);
        printf("URA MI NASHLI VIHOD!!!\n");
        exit(0);
    }

    else if(lab[h][w] == ' ')
    {
        lab[h][w] = 'x';
        printLab(h,w);
        Sleep(500);
        system("cls");
        exit_lab(w - 1, h);
        exit_lab(w, h + 1);
        exit_lab(w + 1, h);
        exit_lab(w, h - 1);
    }
}
int _tmain(int argc, _TCHAR* argv[])
{
    exit_lab(W/2, H/2);
    return 0;
}