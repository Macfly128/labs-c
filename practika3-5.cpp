#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#define DLINA 10

int _tmain(int argc, _TCHAR* argv[])
{
    int sign[DLINA];
    int positive_dig = 0, negative_dig = 0;
    int count = 0, sum = 0;
    int last_PosDig,first_NegDig;
    bool first_ot = true;
    srand(time(NULL));
    while(count < DLINA)
    {
        int rand_dig = rand() % 20 - 10;
        if(rand_dig > 0 && positive_dig < (DLINA / 2))
        {
            sign[count] = rand_dig;
            last_PosDig = count;
            positive_dig++;
            count++;
        }
        else if(rand_dig < 0 && negative_dig < (DLINA / 2))
        {
            sign[count] = rand_dig;
            negative_dig++;

            if(first_ot)
            {
                first_ot = false;
                first_NegDig = count;
            }
            count++;
        }
    }
    printf("Sluchaynie znacheniya massiva\n");

    for(count = 0; count < DLINA; count++)
      printf("%4d", sign[count]);
    
    while(first_NegDig <= last_PosDig)
    {
        sum += sign[first_NegDig];
        first_NegDig++;
    }
    printf("\nSumma mezdu pervim otricatelnim i poslednin polozitelnim %d\n", sum);

    return 0;
}