#include "stdafx.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#define DLINA 80
#define COUNT_WORD 40

void printWord(char *ptr[], int count_ptr)
{
    int count = count_ptr;
    while(count > 0)
    {
        int count_sign = 0;
        int r_digit = rand() % count_ptr;
        if(ptr[r_digit] == 0)
          continue;
        else
          while(isalpha(ptr[r_digit][count_sign]))
            printf("%c", ptr[r_digit][count_sign++]);
        printf(" ");
        ptr[r_digit] = 0;
        count--;
    }
}
int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    char sign[DLINA];
    char *ptr_sign[COUNT_WORD];
    int length, count_ptr = 0;
    bool count = true;
    fgets(sign, DLINA, stdin);
    length = strlen(sign);
    for(int i = 0; i < length; i++)
    {
        if(count == true && isalpha(sign[i]))
        {
            ptr_sign[count_ptr] = &sign[i];
            count_ptr++;
            count = false;
        }
        else if(isspace(sign[i]))
          count = true;
    }
    printWord(ptr_sign, count_ptr);
    return 0;
}