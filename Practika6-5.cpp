#include"stdafx.h"
#include<time.h>

unsigned long int fibonachi(unsigned int n)
{
   if(n == 1 || n == 2)
     return 1;
    else
      return fibonachi(n-2) + fibonachi(n-1);
}
int _tmain(int argc, _TCHAR* argv[])
{
    clock_t begin,end;
    double rfib_time;
    unsigned int count;
    while(!scanf("%u", &count) || (count < 0 || count > 40))
    {
       printf("Vi vveli ne pravilnoe znachenie\n");
       fflush(stdin);
    }
    fflush(stdin);

    begin = clock();
    printf("N-iy element %lu\n", fibonachi(count));
    end = clock();
    rfib_time = (double)(end - begin)/CLOCKS_PER_SEC;
    printf("Vremya vipolneniya %.3lf\n", rfib_time);
    return 0;
}