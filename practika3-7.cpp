#include "stdafx.h"
#define DLINNA 256
int main()
{
    char string[DLINNA];
    int count_s[DLINNA] = {0};
    int count,count_max = 0, sort_count = 0;
    int sign;
    bool first = true;
    scanf("%255s", string);
    for(count = 0; count < DLINNA; count++)
    {
        if(first)
        {
            count_s[string[count]]++;

            if(count_s[count])
              count_max++;
        }

        if(count_s[count] >= sort_count)
        {
            sort_count = count_s[count];
            sign = count;
        }

        if(count == (DLINNA - 1) && count_max > 0)
        {
            printf("%c - %d\n", sign, sort_count);
            count = 0;
            count_max--;
            count_s[sign] = 0;
            sort_count = 0;
            first = false;
        }

    }
    return 0;
}
