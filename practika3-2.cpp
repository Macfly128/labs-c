#include "stdafx.h"
#include <ctype.h>

int _tmain(int argc, _TCHAR* argv[])
{
    char sign;
    int count_word = 0, count_char = 0;
    bool count = true;
    while((sign = getchar()) != EOF)
    {
        if(count == true && isalpha(sign))
          count_word++;

        else if(isspace(sign) || sign == '\n')
        {
          count = true;
          if(count_char > 0)
            printf(" -> kol-vo bukv %d\n", count_char);
          count_char = 0;
          if(sign == '\n')
            break;
          continue;
        }

        count = false;
        count_char++;
        putchar(sign);
    }
    printf("\nKol-vo slov = %d\n", count_word);
    return 0;
}