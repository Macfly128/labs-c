#include "stdafx.h"
#define MAX 1000000

int sequence(int num, int count)
{
    if(num % 2 == 0)
      num = num / 2;

    else
      num = num * 3 + 1;

    count++;

    if(num > 1)
      count = sequence(num, count);
    return count;
}

int _tmain(int argc, _TCHAR* argv[])
{
    int max_seq = 0, number;

    for(int min = 2; min < MAX; min++)
    {
        int count =0;
        count = sequence(min, count);

        if(count > max_seq)
        {
            max_seq = count;
            number = min;
        }
    }
    printf("Samaya dlinnaya posledovatelnost %d i chislo %d\n",max_seq, number);
    return 0;
}