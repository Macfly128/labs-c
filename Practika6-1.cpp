#include "stdafx.h" 
#include <stdio.h>

#define SIZE 31

char canvas[SIZE][SIZE];

void clearCanvas()
{
    int i, j;
    for (i = 0; i < SIZE; i++)
      for (j = 0; j < SIZE; j++)
      {
          canvas[i][j] = ' ';            
      }
}
int power(int k)
{
    int i;
    int val = 1;
    for (i = 0; i < k; i++)
      val *= 3;

    return val;
}
void drawFractal(int x, int y, int k)
{
    if(k == 0)
    {
        canvas[x][y] = '*';
        return;
    }
    else
    {
        canvas[x][y] = '*';
        drawFractal(x, y + power(k-1) ,k-1);
        drawFractal(x, y - power(k-1) ,k-1);
        drawFractal(x, y, k-1);
        drawFractal(x + power(k-1), y ,k-1);
        drawFractal(x - power(k-1), y ,k-1);
    }
}
void showFractal()
{
    int i,j;
    for (i = 0; i < SIZE; i++)
    {
        for (j = 0; j < SIZE; j++)
          putchar(canvas[i][j]);
        putchar('\n');
    }
}
int _tmain(int argc, _TCHAR* argv[])
{
    clearCanvas();
    drawFractal(SIZE / 2 + 1, SIZE / 2 + 1, 3);
    showFractal();
    return 0;
}