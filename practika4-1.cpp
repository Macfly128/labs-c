#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#define DLINA 80

int comp (const void *ptr1, const void *ptr2)
{
    return -(strlen(*(char**)ptr1) - strlen(*(char**)ptr2));
}

int _tmain(int argc, _TCHAR* argv[])
{
    char strings[DLINA][DLINA];
    char *ptr_sort[DLINA];
    int count = 0;

    printf("Vvedite neskolko strok:\n");
    while(count < DLINA && fgets(strings[count], DLINA, stdin) && strings[count][0] != '\n')
    {
        ptr_sort[count] = strings[count];
        count++;
        fflush(stdin);
    }

    qsort(ptr_sort, count, sizeof(char*), comp);

    printf("Otsortirovanniy massiv:\n");
    for(int i = 0; i < count; i++)
      printf("%s", ptr_sort[i]);
    return 0;
}