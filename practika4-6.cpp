#include "stdafx.h"
#include <stdlib.h>

#define DLINA 20
int _tmain(int argc, _TCHAR* argv[])
{
    char (* name)[DLINA];
    int number, age, min_age = 100, max_age = 0;
    char *old, *young;
    printf("Vvedite kol-vo rodstvennikov:\n");
    while(!scanf("%d", &number))
    {
        printf("Vvedite kol-vo rodstvennikov:\n");
        fflush(stdin);
    }
    fflush(stdin);
    name = (char (*)[DLINA]) malloc(number * DLINA *sizeof(char));
    for(int count = 0; count < number; count++)
    {
        printf("VVedite imya rodstvennika:");
        fgets(name[count], DLINA, stdin);
        printf("\nVVedite vozrast:");

        if (!scanf("%d", &age))
        {
            printf("Vivveli nepravilnoe znachenie!!!\n");
            fflush(stdin);
            count -= 1;
            continue;
        }
        else
        {
            fflush(stdin);
            if(age > max_age)
            {
                max_age = age;
                old = name[count];
            }
            if(age <= min_age)
            {
                min_age = age;
                young = name[count]; 
            }
        }
    }
    printf("Samomu staromu chlenu semi %d let i eto %s", max_age, old);
    printf("Samomu molodomu chlenu semi %d let i eto %s", min_age, young);
    return 0;
}