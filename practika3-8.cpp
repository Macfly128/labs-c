#include "stdafx.h"
#include <ctype.h>
#include <string.h>
#define DLINA 80
#define WORD 40

int _tmain(int argc, _TCHAR* argv[])
{
    char sign[DLINA];
    char *arr[WORD];
    int length,count_arr = 0;
    int i;
    bool count = true;
    printf("Vvedite neskolko slov\n");
    fgets(sign, DLINA, stdin);
    length = strlen(sign);
    for(i = 0; i < length; i++)
    {
        if(count == true && isalpha(sign[i]))
        {
            arr[count_arr++] = &sign[i];
            count = false;
        }
        else if(isspace(sign[i]))
        {
            count = true;
            sign[i] = '\0';
        }
    }

    printf("\nKol-vo slov = %d\nViberite slovo ot 1 do %d:", count_arr, count_arr);

    if(int current = scanf("%d", &i) && i > 0 && i <= count_arr)
      printf("%s\n", arr[i-1]);
    else
      printf("Vi vveli nepravilnoe znachenie\n");

    return 0;
}