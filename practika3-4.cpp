#include "stdafx.h"
#include <stdlib.h>
#include <ctype.h>
#define DLINA 80
#define DISCHARGE 4 

int _tmain(int argc, _TCHAR* argv[])
{
    char sign[DLINA];
    char * end;
    int count = 0, sum = 0;
    int digit;
    fgets(sign, DLINA, stdin);
    while(sign[count] != '\n')
    {
        char save_ch;
        save_ch = sign[count + DISCHARGE];
        sign[count + DISCHARGE] = '\0';
        digit = strtol(&sign[count], &end, 10);
        sum += digit;
        sign[count + DISCHARGE] = save_ch;
        count = count + (end - &sign[count]);

        if(isalpha(*end))
          count++;
    }
    printf("%d\n", sum);
    return 0;
}